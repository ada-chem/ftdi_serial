import time
from typing import Any, Union, List, Optional
from ctypes import c_void_p
import threading
import asyncio
import janus
import queue
import atexit
import ftd2xx


Device = ftd2xx.FTD2XX
SerialDeviceType = Union[str, int, ftd2xx.FTD2XX]
NumberType = Union[float, int]


class SerialException(Exception):
    pass


class SerialTimeoutException(SerialException):
    pass


class SerialReadTimeoutException(SerialTimeoutException):
    pass


class SerialWriteTimeoutException(SerialTimeoutException):
    pass


class SerialDeviceInfo:
    def __init__(self,
                 index: int,
                 flags: int,
                 type: int,
                 id: int,
                 location: int,
                 serial: Union[bytes, str],
                 description: Union[bytes, str],
                 handle: c_void_p) -> None:
        self.index = index
        self.flags = flags
        self.type = type
        self.id = id
        self.location = location
        self.handle = handle
        self.serial = serial.decode() if isinstance(serial, bytes) else serial
        self.description = description.decode() if isinstance(description, bytes) else description


class Serial:
    # FT_STATUS
    FT_OK = 0

    # FT_Purge
    FT_PURGE_RX = 1
    FT_PURGE_TX = 2

    @staticmethod
    def list_devices() -> List[SerialDeviceInfo]:
        devices = ftd2xx.listDevices()

        if devices is None:
            return []

        num_devices = len(devices)
        return [SerialDeviceInfo(**ftd2xx.getDeviceInfoDetail(i)) for i in range(0, num_devices)]

    def __init__(self,
                 device: ftd2xx.FTD2XX=None,
                 device_serial: str=None,
                 device_number: int=None,
                 baudrate: int=115200,
                 read_timeout: NumberType=5,
                 write_timeout: NumberType=5,
                 connect_timeout: Optional[NumberType]=30,
                 connect_retry: bool=True,
                 connect_settle_time: NumberType=3,
                 connect: bool=True) -> None:
        self.device = device
        self.device_serial = device_serial
        self.device_number = device_number
        self.baudrate = baudrate
        self.read_timeout_value = read_timeout
        self.write_timeout_value = write_timeout
        self.connect_timeout = connect_timeout
        self.connect_retry = connect_retry
        self.connect_settle_time = connect_settle_time
        self.input_buffer = b''
        self.output_buffer = b''
        self.connected = False

        if connect:
            self.connect()

        atexit.register(self.disconnect)

    def open_device(self):
        if self.device is not None:
            pass
        elif self.device_serial is not None:
            self.device = ftd2xx.openEx(self.device_serial.encode())
        elif self.device_number is not None:
            self.device = ftd2xx.open(self.device_number)
        else:
            self.device = ftd2xx.open()

    def connect(self):
        if self.connected:
            return

        first = True
        start_time = time.time()
        while first or (self.connect_retry and self.device is None):
            try:
                self.open_device()
                # if we weren't able to connect right way, we'll have to wait for the device to settle
                if not first:
                    # wait for the device to settle down
                    time.sleep(self.connect_settle_time)
                    # purge the read buffer to get rid of any bad data accumulated during connection
                    self.device.purge()
                    self.init_device()
            except ftd2xx.DeviceError:
                time.sleep(0.5)

            connect_time = time.time() - start_time

            if self.device is None and connect_time >= self.connect_timeout:
                raise SerialTimeoutException('Timeout while connecting to device')

            first = False

        #time.sleep(0.5)
        #self.init_device()
        self.connected = True

    def disconnect(self):
        if not self.connected:
            return

        self.device.close()
        self.connected = False

    def init_device(self):
        self.device.resetDevice()
        self.device.resetPort()
        self.device.setBaudRate(self.baudrate)
        self.update_timeouts()

    def update_timeouts(self):
        self.device.setTimeouts(int(self.read_timeout_value * 1000), int(self.write_timeout_value * 1000))

    @property
    def info(self) -> SerialDeviceInfo:
        if self.device is None or not self.connected:
            raise SerialException('Cannot get device info, device is not connected')

        return SerialDeviceInfo(**self.device.getDeviceInfo())

    @property
    def serial_number(self)-> str:
        if self.device is None or not self.connected:
            raise SerialException('Cannot get device serial, device is not connected')

        return self.info.serial

    @property
    def in_waiting(self) -> int:
        if self.device is None or not self.connected:
            return 0

        return self.device.getStatus()[0] + len(self.input_buffer)

    @property
    def out_waiting(self) -> int:
        if self.device is None or not self.connected:
            return 0

        return self.device.getStatus()[1]

    @property
    def read_timeout(self):
        return self.read_timeout_value

    @read_timeout.setter
    def read_timeout(self, value):
        self.read_timeout_value = value
        self.update_timeouts()

    @property
    def write_timeout(self):
        return self.write_timeout_value

    @write_timeout.setter
    def write_timeout(self, value):
        self.write_timeout_value = value
        self.update_timeouts()

    def read(self, num_bytes: int=None, timeout: Optional[NumberType]=None) -> bytes:
        if self.device is None or not self.connected:
            raise SerialException('Cannot read, device is not connected')

        if num_bytes is None:
            num_bytes = self.in_waiting

        if num_bytes == 0:
            return b''

        if timeout is not None:
            old_timeout = self.read_timeout
            self.read_timeout = timeout

        serial_data = self.device.read(num_bytes - len(self.input_buffer))

        if timeout is not None:
            self.read_timeout = old_timeout

        if serial_data == b'' and num_bytes != len(self.input_buffer):
            raise SerialReadTimeoutException('Read timeout')

        data = self.input_buffer + serial_data
        self.input_buffer = b''

        return data

    def read_line(self, line_ending: bytes=b'\r', timeout: Optional[NumberType]=None) -> bytes:
        if self.device is None or not self.connected:
            raise SerialException('Cannot read, device is not connected')

        line = b''
        while True:
            num_bytes = self.in_waiting
            # always read at least 1 byte
            bytes_to_read = num_bytes if num_bytes != 0 else 1
            data = self.read(bytes_to_read, timeout=timeout)
            try:
                ending_index = data.index(line_ending)
                [line_data, remaining] = [data[:ending_index + 1], data[ending_index + 1:]]
                self.input_buffer += remaining
                return line + line_data
            except ValueError:
                line += data

    def write(self, data: Union[bytes, str], timeout: Optional[NumberType]=None) -> int:
        if self.device is None or not self.connected:
            raise SerialException('Cannot write, device is not connected')

        if isinstance(data, str):
            data = data.encode()

        if timeout is not None:
            old_timeout = self.write_timeout
            self.write_timeout = timeout

        num_bytes = self.device.write(data)

        if timeout is not None:
            self.write_timeout = old_timeout

        return num_bytes

    def request(self, data: bytes, timeout: Optional[NumberType]=None):
        self.write(data, timeout=timeout)
        return self.read_line(timeout=timeout)

    def flush(self):
        self.device.purge()

    def reset_input_buffer(self):
        self.device.purge(self.FT_PURGE_RX)

    def reset_output_buffer(self):
        self.device.purge(self.FT_PURGE_TX)


class SerialRequest:
    def __init__(self, method: str, *args, **kwargs) -> None:
        self.method = method
        self.args = args
        self.kwargs = kwargs


class AsyncSerial:
    def __init__(self,
                 device: ftd2xx.FTD2XX = None,
                 device_serial: str=None,
                 device_number: int=None,
                 baudrate: int = 115200,
                 event_loop = None,
                 connect_retry=True,
                 connect_timeout=30,
                 connect: bool = True) -> None:
        self.connection = Serial(device=device, device_serial=device_serial, device_number=device_number,
                                 baudrate=baudrate, connect_retry=connect_retry,
                                 connect_timeout=connect_timeout, connect=False)
        self.event_loop = asyncio.get_event_loop() if event_loop is None else event_loop
        self.connect_retry = connect_retry
        self.connect_timeout = connect_timeout
        self.connected = False

        self.read_request_queue: janus.Queue = janus.Queue(loop=self.event_loop)
        self.read_response_queue: janus.Queue = janus.Queue(loop=self.event_loop)
        self.read_thread = threading.Thread(target=self.request_thread_handler,
                                            args=(self.read_request_queue.sync_q, self.read_response_queue.sync_q))

        self.write_request_queue: janus.Queue = janus.Queue(loop=self.event_loop)
        self.write_response_queue: janus.Queue = janus.Queue(loop=self.event_loop)
        self.write_thread = threading.Thread(target=self.request_thread_handler,
                                             args=(self.write_request_queue.sync_q, self.write_response_queue.sync_q))

        if connect:
            self.connect_sync()

        atexit.register(self.disconnect_sync)

    def connect_sync(self):
        if self.connected:
            return

        self.connected = True
        self.read_thread.start()
        self.write_thread.start()
        self.connection.connect()

    def disconnect_sync(self):
        if not self.connected:
            return

        self.connected = False
        self.connection.disconnect()

    async def connect(self):
        if self.connected:
            return

        await self.event_loop.run_in_executor(None, self.connect_sync)

    async def disconnect(self):
        if not self.connected:
            return

        await self.event_loop.run_in_executor(None, self.disconnect_sync)

    def request_thread_handler(self, request_queue: queue.Queue, response_queue: queue.Queue):
        while self.connected:
            if not self.connection.connected:
                time.sleep(0.01)
                pass

            try:
                request: SerialRequest = request_queue.get(timeout=1.0)
                method = getattr(self.connection, request.method)
                try:
                    response = method(*request.args, **request.kwargs)
                    response_queue.put(response)
                except Exception as error:
                    response_queue.put(error)
            except queue.Empty:
                pass

    async def queue_request(self, request_queue: janus.Queue, response_queue: janus.Queue, request: SerialRequest) -> Any:
        await request_queue.async_q.put(request)
        response = await response_queue.async_q.get()

        if isinstance(response, Exception):
            raise response

        return response

    async def queue_read_request(self, request: SerialRequest) -> Any:
        return await self.queue_request(self.read_request_queue, self.read_response_queue, request)

    async def queue_write_request(self, request: SerialRequest) -> Any:
        return await self.queue_request(self.write_request_queue, self.write_response_queue, request)

    async def write(self, data: bytes, timeout: Optional[NumberType]=None):
        num_bytes_written = await self.queue_write_request(SerialRequest('write', data, timeout=timeout))
        if num_bytes_written != len(data):
            raise SerialException(f'Number of written bytes ({num_bytes_written}) does not equal data length ({len(data)})')

    async def read(self, num_bytes: int=None, timeout: Optional[NumberType]=None) -> bytes:
        data = await self.queue_read_request(SerialRequest('read', num_bytes, timeout=timeout))
        if num_bytes is not None and len(data) != num_bytes:
            raise SerialException(f'Number of bytes read ({len(data)}) is not {num_bytes}')

        return data

    async def read_line(self, timeout: Optional[NumberType]=None) -> bytes:
        return await self.queue_read_request(SerialRequest('read_line', timeout=timeout))

    async def request(self, data: bytes, timeout: Optional[NumberType]=None) -> bytes:
        await self.write(data, timeout=timeout)
        return await self.read_line(timeout=timeout)
